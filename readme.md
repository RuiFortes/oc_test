oc new-project test

oc policy add-role-to-user admin developer -n test

lerna bootstrap --hoist --loglevel=verbose

TO FIGURE OUT: where is `oc` path defined in minishift

To use hostpath we need to [Use the hostPath Volume Plug-in](https://docs.openshift.org/latest/admin_guide/manage_scc.html#use-the-hostpath-volume-plugin)
[hostPath not allowed in templates](https://github.com/minishift/minishift/issues/879)
[openshift use hostPath error](https://github.com/openshift/origin/issues/11153)
