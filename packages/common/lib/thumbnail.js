'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _gm = require('gm');

var _gm2 = _interopRequireDefault(_gm);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class Thumbnail {
  constructor(originalsFolder, thumbnailsFolder, supportedImageTypes) {

    this.supportedImageTypes = supportedImageTypes || ['png', 'jpg', 'jpeg', 'gif'];

    if (!Array.isArray(this.supportedImageTypes)) throw new TypeError('supportedImageTypes must be an array');

    this._supportedExtensions = this.supportedImageTypes.map(item => `.${item}`);

    this.originalsFolder = originalsFolder;
    this.thumbnailsFolder = thumbnailsFolder;
  }

  ensureThumbnail(filename, width = '', height = '') {
    return new _promise2.default((resolve, reject) => {

      if (!filename) throw new Error('provide filename');

      if (!width || !height) throw new Error('provide width and height');

      if (typeof filename !== 'string') throw new TypeError('filename must be a string');
      if (width && typeof width !== 'number') throw new TypeError('width must be a number');
      if (height && typeof height !== 'number') throw new TypeError('height must be a number');

      if (filename !== _path2.default.basename(filename)) throw new Error('filename contains a path');

      var extension = _path2.default.extname(filename);
      if (!(this._supportedExtensions.indexOf(extension.toLowerCase()) !== -1)) throw new TypeError('image type not supported');

      var filePath = `${this.originalsFolder}/${filename}`;
      var thumbPath = `${this.thumbnailsFolder}/${_path2.default.basename(filename, extension)}-${width}x${height}${extension}`;

      _fs2.default.stat(thumbPath, (err, stats) => {
        if (err) {
          (0, _gm2.default)(filePath).resize(width, height).gravity('Center').extent(width, height).noProfile().write(thumbPath, err => err ? reject(err) : resolve(thumbPath));
        } else {
          resolve(thumbPath);
        }
      });
    });
  }

}
exports.default = Thumbnail;