import path from 'path'
import fs from 'fs'
import gm from 'gm' // http://aheckmann.github.io/gm/docs.html

export default class Thumbnail {
  constructor(originalsFolder, thumbnailsFolder, supportedImageTypes) {

    this.supportedImageTypes = supportedImageTypes || [ 'png', 'jpg', 'jpeg', 'gif' ]

    if (!Array.isArray(this.supportedImageTypes)) throw new TypeError('supportedImageTypes must be an array')

    this._supportedExtensions = this.supportedImageTypes.map(item => `.${item}`)

    this.originalsFolder = originalsFolder
    this.thumbnailsFolder = thumbnailsFolder
  }

  ensureThumbnail(filename, width = '', height = '') {
    return new Promise((resolve, reject) => {

      if (!filename) throw new Error('provide filename')
      // if (!width && !height) throw new Error('provide width and/or height')
      if (!width || !height) throw new Error('provide width and height')

      if (typeof filename !== 'string') throw new TypeError('filename must be a string')
      if (width && typeof width !== 'number') throw new TypeError('width must be a number')
      if (height && typeof height !== 'number') throw new TypeError('height must be a number')

      if (filename !== path.basename(filename)) throw new Error('filename contains a path')

      var extension = path.extname(filename)
      if(!this._supportedExtensions.includes(extension.toLowerCase())) throw new TypeError('image type not supported')

      var filePath = `${this.originalsFolder}/${filename}`
      var thumbPath = `${this.thumbnailsFolder}/${path.basename(filename, extension)}-${width}x${height}${extension}`

      fs.stat(thumbPath, (err, stats) => {
        if(err) {
          gm(filePath)
          .resize(width, height)
          .gravity('Center')
          .extent(width, height)
          .noProfile()
          .write(thumbPath, err => err ? reject(err) : resolve(thumbPath) )
        } else {
          resolve(thumbPath)
        }
      })

    })

  }

}
