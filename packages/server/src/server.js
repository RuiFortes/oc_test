var express = require('express'),
    app     = express(),
    fs      = require('fs'),
    ejs     = require('ejs'),
    path    = require('path')

var alpha = require('alpha')
var Thumbnail = require('../common/thumbnail').default

var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080
var ip = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0'
var imageFolder = (process.env.DATA_DIR || '/media/rsf/Acer/Users/User/Documents/WEBDEV/RALPI/ralpi_site/data') + '/img'

app.engine('html', ejs.renderFile)
app.set('views', __dirname + '/views')

var thumbnail_pecas = new Thumbnail(imageFolder + '/pecas', imageFolder + '/thumbs/pecas')
var acceptedSizes = ['60x68', '80x80', '350x350']

app.get('/img/part/:id', function(req, res) {

  const filename = req.params.id + (path.extname(req.params.id) !== '.jpg' ? '.jpg' : '')

  if(req.query.size == null) return res.sendFile(imageFolder + '/pecas/' + filename)

  if(acceptedSizes.indexOf(req.query.size) < 0) return res.status(401).send('no thumb for that size')

  var dims = req.query.size.split('x').map(v => parseInt(v, 10))

  thumbnail_pecas.ensureThumbnail(filename, dims[0], dims[1])
  .then(thumbPath => {
    res.sendFile(thumbPath)
  })
  .catch(err => {
    console.log(err)
    res.sendStatus(404)
  })

})

app.get('/', function (req, res) {
  return res.render('index.html', { pageCountMessage : null});
})



// error handling
app.use(function(err, req, res, next){
  console.error(err.stack)
  res.status(500).send('Something bad happened!')
})

app.listen(port, ip, () => console.log(`Server listening on ${ip}:${port}`))
